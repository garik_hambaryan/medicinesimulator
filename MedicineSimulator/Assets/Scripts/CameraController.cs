﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraController : MonoBehaviour
{

    //public Camera m_mainCamera;

    ImageGenerator m_imageGenerator;

    public GameObject m_lTOb;
    public GameObject m_lBOb;
    public GameObject m_rTOb;
    public GameObject m_rBOb;

    void startAnimation()
    {
        this.gameObject.transform.DOMoveX((this.gameObject.transform.position.x + 0.05f), 1).OnUpdate(onUpdateMoving).OnComplete(onCompleteMoving);
    }
    void onUpdateMoving()
    {
        print("onUpdateMoving");

    }
    void onCompleteMoving()
    {


    }

    Camera m_guiderCamera;


    MainController m_mainController;
	void Start ()
    {
        m_mainController = GameObject.Find("_manager").GetComponent<MainController>();
        m_imageGenerator = GameObject.Find("_manager").GetComponent<ImageGenerator>();
        m_guiderCamera = GameObject.Find("GuiderCamera").GetComponent<Camera>();
        fixMostLowerLeftPosition();
        
        //startAnimation();
    }
    float step = 0.03f;
    void fixMostLowerLeftPosition()
    {
        print("Camera Before : " + Camera.main.transform.position);     
       
        while (checkDataMatrixInsideFrustum())
        {
            m_guiderCamera.transform.position = m_guiderCamera.transform.position - new Vector3(step, 0, 0);            
        }
        m_guiderCamera.transform.position = m_guiderCamera.transform.position + new Vector3(2 * step, 0, 0);        
        print("Camera After X : " + m_guiderCamera.transform.position);
        while (checkDataMatrixInsideFrustum())
        {
            m_guiderCamera.transform.position = m_guiderCamera.transform.position - new Vector3(0, step, 0);           
        }
        m_guiderCamera.transform.position = m_guiderCamera.transform.position + new Vector3(0, 2 * step, 0);
        lookAtDataMatrixCenter();

        print("Camera After Y : " + m_guiderCamera.transform.position);      
    }

    GameObject m_objectForLookAt;
    public void setObjectForLookAt(GameObject obj)
    {
        m_objectForLookAt = obj;
    }

   private bool checkDataMatrixInsideFrustum()
    {
        if(!checkGameObjectOriginInsideMainCameraFrustum(m_lTOb))
        {
            return false;
        }
        if (!checkGameObjectOriginInsideMainCameraFrustum(m_lBOb))
        {
            return false;
        }
        if (!checkGameObjectOriginInsideMainCameraFrustum(m_rTOb))
        {
            return false;
        }
        if (!checkGameObjectOriginInsideMainCameraFrustum(m_rBOb))
        {
            return false;
        }
        return true;
    }

    bool checkGameObjectOriginInsideMainCameraFrustum(GameObject obj)
    { 
        Vector3 v = m_guiderCamera.WorldToViewportPoint(obj.transform.position);

        if (v.x* v.y >= 0 && v.x <= 1.0 && v.y <= 1.0 && v.z >= 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    enum MovementDirection
    {
        eLeft,
        eRight,
        eUp,
        eDown,
    }

    MovementDirection m_currentHorizontalMovementDirection = MovementDirection.eLeft;
    MovementDirection m_currentVerticalMovementDirection = MovementDirection.eUp;

    float getStepByOrientation(MovementDirection m)
    {
        
        if (m == MovementDirection.eLeft || m == MovementDirection.eUp)
        {
            return step;
        }
        else
        {
            return -step;
        }
    }
    public void iterateNextMovement()
    {        
            m_guiderCamera.transform.position = m_guiderCamera.transform.position + new Vector3(getStepByOrientation(m_currentHorizontalMovementDirection), 0, 0);
            if(!checkDataMatrixInsideFrustum())
            {
                m_guiderCamera.transform.position = m_guiderCamera.transform.position - new Vector3(getStepByOrientation(m_currentHorizontalMovementDirection), 0, 0);
                m_currentHorizontalMovementDirection = m_currentHorizontalMovementDirection == MovementDirection.eLeft ? MovementDirection.eRight : MovementDirection.eLeft;
                m_guiderCamera.transform.position = m_guiderCamera.transform.position + new Vector3(0, getStepByOrientation(m_currentVerticalMovementDirection), 0);

               if (!checkDataMatrixInsideFrustum())
               {
                    m_guiderCamera.transform.position = m_guiderCamera.transform.position - new Vector3(0, getStepByOrientation(m_currentVerticalMovementDirection), 0);
                    m_currentVerticalMovementDirection = (m_currentVerticalMovementDirection == MovementDirection.eUp ? MovementDirection.eDown : MovementDirection.eUp);
               }
            
            }
            lookAtDataMatrixCenter();

    }
    void lookAtDataMatrixCenter()
    {
        Camera.main.transform.LookAt(0.25f * (m_lTOb.transform.position + m_lBOb.transform.position + m_rTOb.transform.position + m_rBOb.transform.position));
    }

    float m_currentScaleTarget = 2;
    float m_scaleLowerBoundary = 2;

    float m_scaleHigherBoundary = 4;

    public void iterateNextScale()
    {
        m_objectForLookAt.transform.localScale = new Vector3(m_objectForLookAt.transform.localScale.x, Mathf.Lerp(m_objectForLookAt.transform.localScale.y, m_currentScaleTarget, 0.2f), m_objectForLookAt.transform.localScale.z);
        if(Mathf.Abs(m_objectForLookAt.transform.localScale.y - m_currentScaleTarget) < 0.01)
        {
            m_currentScaleTarget = Random.Range(m_scaleLowerBoundary, m_scaleHigherBoundary);

        }
    }

    float m_cameraMinZPosition = -0.125f;
    float m_cameraMaxZPosition = 0.03f;
    float m_cameraCurrentZPosition = 0;
    public void iterateNextZValue()
    {
        Vector3 dumpCurrentPos = m_guiderCamera.transform.position;
        m_guiderCamera.transform.position = new Vector3(m_guiderCamera.transform.position.x, m_guiderCamera.transform.position.y, Mathf.Lerp(m_guiderCamera.transform.position.z, m_cameraCurrentZPosition, 0.2f));
        if(!checkDataMatrixInsideFrustum())
        {
            m_guiderCamera.transform.position = dumpCurrentPos;
            m_cameraCurrentZPosition = Random.Range(m_cameraMinZPosition, m_cameraMaxZPosition);
        } 
        if(Mathf.Abs(m_cameraCurrentZPosition - dumpCurrentPos.z) < 0.01)
        {
            if (m_cameraCurrentZPosition < 0)
            {
                m_cameraCurrentZPosition = m_cameraMaxZPosition;
            }
            else
            {
                m_cameraCurrentZPosition = Random.Range(m_cameraMinZPosition, m_cameraMaxZPosition);
            }
        }
    }

    public Light m_directionalLight;

    float m_currentIntensityVal = 3;
    float m_lightIntensityMinVal = 0.1f;
    float m_lightIntensityMaxVal = 4;

    public void iterateLightIntencity()
    {
        m_directionalLight.intensity = Mathf.Lerp(m_directionalLight.intensity, m_currentIntensityVal, 0.2f);
        if (Mathf.Abs(m_directionalLight.intensity - m_currentIntensityVal) < 0.01)
        {
            m_currentIntensityVal = Random.Range(m_lightIntensityMinVal, m_lightIntensityMaxVal);
        }
    }

    float m_spotlightIntensityMinVal = 0;
    float m_spotlightIntensityMaxVal = 3;
    float m_spotlightIntensityCurrentVal = 1;
    public Light m_spotLight;
    public void iterateSpotLightIntensity()
    {
        m_spotLight.intensity = Mathf.Lerp(m_spotLight.intensity, m_spotlightIntensityCurrentVal, 0.2f);
        if (Mathf.Abs(m_spotLight.intensity - m_spotlightIntensityCurrentVal) < 0.01)
        {
            m_spotlightIntensityCurrentVal = Random.Range(m_spotlightIntensityMinVal, m_spotlightIntensityMaxVal);
        }
    }



    public void iterateNoiseLevel()
    {

    }

    Color m_currentTargetColor = new Color(133, 28, 28);
    Color m_newTargetColor = new Color(0, 0, 0);
    float m_colorChangeDuration = 0.2f;
    public void iterateTargetRandomColors()
    {
        m_objectForLookAt.GetComponent<MeshRenderer>().material.color = new Color(Mathf.Lerp(m_objectForLookAt.GetComponent<MeshRenderer>().material.color.r, m_currentTargetColor.r, m_colorChangeDuration),
            Mathf.Lerp(m_objectForLookAt.GetComponent<MeshRenderer>().material.color.g, m_currentTargetColor.g, m_colorChangeDuration),
            Mathf.Lerp(m_objectForLookAt.GetComponent<MeshRenderer>().material.color.b, m_currentTargetColor.b, m_colorChangeDuration));

        if ((m_objectForLookAt.GetComponent<MeshRenderer>().material.color.r - m_currentTargetColor.r)<0.01 &&
            (m_objectForLookAt.GetComponent<MeshRenderer>().material.color.g - m_currentTargetColor.g)<0.01 &&
            (m_objectForLookAt.GetComponent<MeshRenderer>().material.color.b - m_currentTargetColor.b)<0.01)
        {
            m_currentTargetColor = Random.ColorHSV();
        }
    }




    void Update ()
    {
        /*if (checkDataMatrixInsideFrustum())
        {
            print("Target Visible");
        }
        else
        {
            print("Target Invisible");
        }*/
    }

    public void canStartImageShooting(bool b)
    {
        m_canTakeScreenShot = b;
    }
    bool m_canTakeScreenShot = false;
    int m_onPostrenderCount = 0;
    int m_imageIndex = 0;
    private void OnPostRender()
    {
        if (!m_canTakeScreenShot)
        {
            return;
        }
        m_onPostrenderCount++;

        if (m_onPostrenderCount % 20 == 0)
        {
            m_imageIndex++;
            m_imageGenerator.generateImageFromCamera(m_imageIndex);
            print("Image Generated: " + m_imageIndex);
        }        
    }
}
