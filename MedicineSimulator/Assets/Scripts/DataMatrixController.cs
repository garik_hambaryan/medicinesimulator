﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataMatrixController : MonoBehaviour
{

    public List<Texture2D> m_dataMatrixTextures = new List<Texture2D>();

	void Start ()
    {
        Texture2D[] dmi = Resources.LoadAll<Texture2D>("DataMatrixImages/");
        for (int i = 0; i < dmi.Length; i++)
        {
            m_dataMatrixTextures.Add(dmi[i]);
        }
	}

    GameObject m_dataMatrixField;
    void dataMatrixRandomChangeInvoker()
    {
        if (m_dataMatrixField != null)
        {
            int rndVal = Random.Range(0, m_dataMatrixTextures.Count);
            print("rndVal: : : : : : : : " + rndVal);
            m_dataMatrixField.GetComponent<MeshRenderer>().material.mainTexture = m_dataMatrixTextures[rndVal];
        }
        else
        {
            print("ERROR: No DataMatrixField");
        }        
    }

    public void startDataMatrixChangingRandomly(GameObject dataMatrixField)
    {
        m_dataMatrixField = dataMatrixField;
        InvokeRepeating("dataMatrixRandomChangeInvoker", 1, 1.5f);        
    }



    int m_currentDataMatrixIndex = -1;
    void dataMatrixQueuedChangeInvoker()
    {
        // int maxElementIndex = m_dataMatrixTextures.Count;

        if (m_currentDataMatrixIndex < m_dataMatrixTextures.Count - 1)
        {
            m_currentDataMatrixIndex++;
        }
        else
        {
            m_currentDataMatrixIndex = 0;
        }        

        if (m_dataMatrixField != null)
        {
            //print("Invoking");
            m_dataMatrixField.GetComponent<MeshRenderer>().material.mainTexture = m_dataMatrixTextures[m_currentDataMatrixIndex];
        }
        else
        {
            print("ERROR: No DataMatrixField");
        }
    }
    public void startDataMatrixChangingQueued(GameObject dataMatrixField)
    {
        m_dataMatrixField = dataMatrixField;
        InvokeRepeating("dataMatrixQueuedChangeInvoker", 1, 1.5f);
    }

    public int getDataMatrixImageCount()
    {
        return m_dataMatrixTextures.Count;
    }

    public Texture2D getDataMatrixImage(int n)
    {
        return m_dataMatrixTextures[n];
    }

	void Update ()
    {
		
	}
}
