﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MainController : MonoBehaviour
{
    CameraController m_cameraController;
    ImageGenerator m_imageGenerator;
    DataMatrixController m_dataMatrixController;

    public GameObject m_target;



    void Awake ()
    {
        m_cameraController = Camera.main.GetComponent<CameraController>();
        m_imageGenerator = GameObject.Find("_manager").GetComponent<ImageGenerator>();
        m_dataMatrixController = GameObject.Find("_manager").GetComponent<DataMatrixController>();

    }

    private void Start()
    {
        m_cameraController.setObjectForLookAt(m_target);
        m_dataMatrixController.startDataMatrixChangingRandomly(m_target.transform.Find("QRField").gameObject);
        //m_dataMatrixController.startDataMatrixChangingQueued(m_target.transform.Find("QRField").gameObject);
        m_cameraController.canStartImageShooting(true);
    }


    void Update ()
    {
        m_cameraController.iterateNextMovement();
        m_cameraController.iterateNextScale();
        m_cameraController.iterateNextZValue();
        m_cameraController.iterateLightIntencity();
        m_cameraController.iterateNoiseLevel();
        m_cameraController.iterateTargetRandomColors();
        m_cameraController.iterateSpotLightIntensity();
    }
}
